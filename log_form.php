<?php
session_start();

require_once "./src/function/Database.php";
require_once "src/models/User.php";

require_once "src/controller/UserController.php";

$userController = new UserController();

// var_dump($_POST);

$user_email = null;
$user_pass = null;

// je vérifie que la clé existe dans le tableau POST (depuis le formulaire de log)
if (isset($_POST['email'])) {
    $user_email = $_POST['email'];
}

if (isset($_POST['password'])) {
    $user_pass = $_POST['password'];
}

// var_dump($user_email);
// si mes 2 variables existes
if(isset($user_email) && isset($user_pass)){
    // je vérifie que l'utilisateur existe en BDD
    $res = $userController->checkLogin($user_email, $user_pass);
    // Si j'ai une réponse positive
    if($res['response']){
        
        $user = $res['user'];
        $_SESSION['user_name'] = $user->getName();
        $_SESSION['user_email'] = $user->getEmail();
        
        header('Location:afficher_films.php');
    } else {
        echo "Cet utilisateur n'existe pas";
    }

}

include_once 'header.inc.php';

?>
<h2>Connexion</h2>

<form action="#" method="POST">
    <input type="text" name="email">
    <input type="password" name="password">
    <input type="submit" value="Se connecter">
</form>


<?php
include_once 'footer.inc.php';
